require "test_helper"

# class CategoriesControllerTest < ActionController::TestCase
class CategoriesControllerTest < ActionDispatch::IntegrationTest

    def setup
        @category = Category.create(name: "sports")
        @user = User.create(username: "andreas", email: "andreas@example.com", password: "123123", admin: "true")
    end

    test "should get index categories index" do
        # get :index
        get categories_path
        assert_response :success
    end

    test "should get new" do
        # session[:user_id] = @user.id
        # get :new
        sign_in_as(@user, "123123")
        get new_category_path
        assert_response :success
    end

    test "should get show" do
        # get (:show, { id: @category.id })
        get category_path(@category)
        assert_response :success
    end

    test "should redirect create when admin not logged in" do
        assert_no_difference "Category.count" do
            # post :create, category: { name: "sports" }
            post categories_path, params: { category: { name: "sports" } }
        end

        assert_redirected_to categories_path
    end
end
