require "test_helper"

class ListCategoriesTest < ActionDispatch::IntegrationTest

    def setup
        @category = Category.create(name: "sports")
        @category2 = Category.create(name: "programming")
    end

    test "should show category listing" do
        get categories_path
        # assert_template moved into gem 'rails-controller-testing'
        # assert_template 'categories/index'
        # using rake test = success (rake test command just for create_categories_test.rb)
        # using rails test = failed (rails test command for the whole project from the beginning)
        assert_select "a[href = ?]", category_path(@category), text: @category.name
        assert_select "a[href = ?]", category_path(@category2), text: @category2.name
    end
end
