require "test_helper"

class CreateCategoriesTest < ActionDispatch::IntegrationTest

    def setup
        @user = User.create(username: "andreas", email: "andreas@example.com", password: "123123", admin: "true")
    end

    test "get new category form and create category" do
        sign_in_as(@user, "123123")
        get new_category_path
        # assert_template moved into gem 'rails-controller-testing'
        # assert_template 'categories/new'
        # using rake test = success (rake test command just for create_categories_test.rb)
        # using rails test = failed (rails test command for the whole project from the beginning)
        assert_difference "Category.count", 1 do
            # post_via_redirect categories_path, category: { name: "sports" }
            post categories_path, params: { category: { name: "sports" } }
            follow_redirect!
        end
        # assert_template 'categories/index'
        assert_match "sports", response.body
    end

    test "invalid category submission results in failure" do
        sign_in_as(@user, "123123")
        get new_category_path
        # assert_template moved into gem 'rails-controller-testing'
        # assert_template 'categories/new'
        # using rake test = success (rake test command just for create_categories_test.rb)
        # using rails test = failed (rails test command for the whole project from the beginning)
        assert_no_difference "Category.count" do
            # post_via_redirect categories_path, category: { name: " " }
            post categories_path, params: { category: { name: " " } }
        end
        # assert_template 'categories/new'
        assert_select "h2.panel-title"
        assert_select "div.panel-body"
    end
end
